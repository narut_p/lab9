package lab9;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * A main method.
 * @author Narut Poovorakit
 * @version 24.03.2015
 *
 */
public class Main {
	public static void main(String[] args) {
		/** Print the source of the word */
		System.out.println("Reading words from http://se.cpe.ku.ac.th/dictionary");

		/** Initialize Stopwatch */
		StopWatch st = new StopWatch();
		
		/** Start the timer */
		st.start();
		final String DICT_URL = "http://se.cpe.ku.ac.th/dictionary.txt";
		URL url = null;
		try {
			url = new URL(DICT_URL);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		WordCounter counter = new WordCounter();
		int wordcount = counter.countWords(url);
		int syllables = counter.getSyllableCount();

		/** Print a number of syllables and words */
		System.out.printf("Counted %,d syllables in %,d words\n", syllables, wordcount);

		/** Stop the timer */
		st.stop();
		/** Print a elapsed time */
		System.out.printf("Elapsed time: %.3f sec", st.getElapsed());
	}

}
