package lab9;

/**
 * An enum class.
 * @author Narut Poovorakit
 * @version 24.03.2015
 *
 */
public enum State {
	START,CONSONANT,E_FIRST,VOWELS,NON_WORD,DASH;
	
	private State () {
		
	}
}
