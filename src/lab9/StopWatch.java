package lab9;
/**
 * A Stopwatch class
 * @author Narut Poovorakit
 * @version 31.01.2015
 *
 */
public class StopWatch {
	//create a time that a beginning.
	private long startTime;
	//create a time in the end.
	private long stopTime;
	//create check that run or not.
	private boolean checkRun = false;
	//create a number in nanosecond.
	private static final double NANOSECONDS = 1.0E+9;

	/**
	 * 
	 * @return if the time is running, its return the time between current and the beginning.
	 * but if not, its return the time between end time and beginning.
	 * 
	 */
	public double getElapsed() {
		if (isRunning()) {
			return (System.nanoTime() - startTime) / NANOSECONDS;
		} else {
			return (stopTime - startTime) / NANOSECONDS;
		}
	}

	/**
	 * 
	 * @return return true if the time is running.
	 * but it return false if its not running.
	 */
	public boolean isRunning() {
		if (checkRun) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * if the time is stop the beginning of the time will be the current time.
	 * and check run to be true.
	 */
	public void start() {
		if (!isRunning()) {
			startTime = System.nanoTime();
			checkRun = true;
		}
	}

	/**
	 * if the time is still run the stop time will be set to be the current time.
	 * and check run to be true;
	 */
	public void stop() {
		if (isRunning()) {
			stopTime = System.nanoTime();
			checkRun = false;
		}
	}

}
