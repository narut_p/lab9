package lab9;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Scanner;

/**
 * A class that count a word.
 * 
 * @author Narut Poovorakit
 * @version 24.03.2015
 *
 */
public class WordCounter {
	/** An enum of state */
	State state;

	/** a number of syllable */
	private int getSyllable = 0;

	/**
	 * A method that count a syllables of word.
	 * 
	 * @param word
	 *            is a word that send from main.
	 * @return a number that count from word.
	 */
	public int countSyllables(String word) {

		int syllable = 0;
		state = State.START;
		String Word = word.replaceAll("'", "");
		for (char c : Word.toCharArray()) {
			switch (state) {

			case START:
				if (c == 'e' || c == 'E') {
					state = State.E_FIRST;
				} else if (isVowel(c)) {
					state = State.VOWELS;
					syllable++;
				} else if (Character.isLetter(c))
					state = State.CONSONANT;
				else
					state = State.NON_WORD;
				break;

			case CONSONANT:

				if (c == 'e' || c == 'E') {
					state = State.E_FIRST;
				} else if (isVowel(c) || Character.toUpperCase(c) == 'Y') {
					state = State.VOWELS;
					syllable++;
				} else if (Character.isLetter(c) || c == '-')
					state = State.CONSONANT;
				else
					state = State.NON_WORD;

				break;

			case E_FIRST:

				if (isVowel(c)) {
					state = State.VOWELS;
					syllable++;
				} else if (c == '-') {
					state = State.CONSONANT;
				} else if (Character.isLetter(c)) {
					state = State.CONSONANT;
					syllable++;
				}

				break;

			case VOWELS:
				if (isVowel(c))
					state = State.VOWELS;
				else if (Character.isLetter(c) || '-' == c)
					state = State.CONSONANT;
				else
					state = State.NON_WORD;
				break;
			}
		}
		if (state.equals(State.CONSONANT)) {
			if (Word.charAt(Word.length() - 1) == '-')
				syllable = 0;
		}
		if (syllable == 0 && state == State.E_FIRST)
			syllable = 1;
		if (state == State.NON_WORD)
			syllable = 0;

		return syllable;
	}

	/**
	 * A method that count a word
	 * 
	 * @param instream
	 *            is a source that sended.
	 * @return the number of word.
	 */
	public int countWords(InputStream instream) {
		int count = 0;
		Scanner scan = new Scanner(instream);
		while (scan.hasNext()) {
			count++;
			getSyllable += countSyllables(scan.next());

		}
		return count;
	}

	/**
	 * 
	 * @param url
	 *            is a link that sended.
	 * @return the number of word that be counting.
	 */
	public int countWords(URL url) {
		try {
			return countWords(url.openStream());
		} catch (IOException e) {
			e.printStackTrace();
			return 0;
		}
	}

	/**
	 * 
	 * @param c
	 *            is a letter that been sending.
	 * @return true if it was a vowels. false if it was it isn't a vowels.
	 */
	public boolean isVowel(char c) {
		String vowel = "AEIOUaeiou";
		for (char charVowel : vowel.toCharArray()) {
			if (c == charVowel)
				return true;
		}
		return false;
	}

	/**
	 * 
	 * @return a syllable.
	 */
	public int getSyllableCount() {

		return getSyllable;
	}

}
